use std::cell::Ref;

use crate::automata::cells::Cell;
use grid::Grid;
use tui::buffer::Buffer;
use tui::layout::Rect;
use tui::widgets::{StatefulWidget, Widget};
use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Alignment, Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::Span,
    widgets::{Block, BorderType, Borders},
    Frame, Terminal,
};

pub fn ui<B: Backend>(f: &mut Frame<B>, petri_state: &mut PetriState) {
    let size = f.size();
    let ui_columns = Layout::default()
        .direction(Direction::Horizontal)
        .margin(1)
        .constraints([Constraint::Percentage(15), Constraint::Percentage(100)])
        .split(size);
    let block = Block::default()
        .borders(Borders::ALL)
        .title("Tools")
        .title_alignment(Alignment::Center)
        .border_type(BorderType::Rounded);
    let mut block2 = Petri::default().block(
        Block::default()
            .borders(Borders::ALL)
            .title_alignment(Alignment::Center)
            .border_type(BorderType::Rounded),
    );
    f.render_widget(block, ui_columns[0]);
    f.render_stateful_widget(&mut block2, ui_columns[1], petri_state);
}
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum GameMode {
    Play,
    Edit,
}

pub struct PetriState {
    pub cursorpos: (usize, usize),
    pub blk: bool,
    pub mode: GameMode,
    pub cells: Grid<Cell>,
    pub offset: (usize, usize),
}

impl PetriState {
    pub fn new(cells: Grid<Cell>) -> PetriState {
        PetriState {
            cursorpos: (0, 0),
            mode: GameMode::Play,
            blk: true,
            cells,
            offset: (10, 10),
        }
    }
}

// Get it cause... cells grow in petri dishes?
#[derive(Default)]
pub(crate) struct Petri<'a> {
    /// A block to wrap the widget in
    block: Option<Block<'a>>,
}

impl<'a> Petri<'a> {
    pub fn block(mut self, block: Block<'a>) -> Petri<'a> {
        self.block = Some(block);
        self
    }
}

impl<'a> StatefulWidget for &mut Petri<'a> {
    type State = PetriState;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        let petri_area = match self.block.take() {
            Some(b) => {
                let inner_area = b.inner(area);
                b.render(area, buf);
                inner_area
            }
            None => area,
        };

        for x in 0..petri_area.width {
            for y in 0..(petri_area.height) {
                let top_color = if let Some(cell) = state.cells.get(x as usize, y as usize * 2) {
                    cell.color()
                } else {
                    Color::Black
                };

                let bottom_color =
                    if let Some(cell) = state.cells.get(x as usize, y as usize * 2 + 1) {
                        cell.color()
                    } else {
                        Color::Black
                    };

                buf.get_mut(petri_area.left() + x as u16, petri_area.top() + y as u16)
                    .set_style(Style::default().bg(bottom_color).fg(top_color))
                    .set_symbol("▀");
            }
        }

        if state.mode == GameMode::Edit && state.blk {
            let curosr;

            let x = state.cursorpos.0;
            let y = state.cursorpos.1;

            let bg;

            if y % 2 == 0 {
                curosr = "⠛";
                bg= if let Some(cell) = state.cells.get(x as usize, y + 1) {cell.color()} else {Color::Magenta};
            } else {
                curosr = "⣤";
                bg= if let Some(cell) = state.cells.get(x as usize, y - 1) {cell.color()} else {Color::Magenta};
            }


            buf.get_mut(
                petri_area.left() + x as u16,
                petri_area.top() + y as u16 / 2,
            )
            .set_bg(bg)
            .set_fg(Color::White)
            .set_symbol(curosr);
        }
    }
}
