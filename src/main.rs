mod automata;
mod interface;

use automata::{cells::Cell, world::World};
use crossterm::{
    event::{
        self, poll, read, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEventKind,
    },
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use std::io::{self, stdout, Stdout};
use tui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};

use interface::{ui, GameMode, PetriState};
use portmidi as pm;
use std::{thread, time};

use rand::prelude::*;

enum Msg {
    SetCell { x: usize, y: usize, cell: Cell },
    Fill(Cell),
}

struct App {
    mode: GameMode,
    world: World,
    msgs: Vec<Msg>,
    terminal: Terminal<CrosstermBackend<Stdout>>,
    petri_state: PetriState,
    frame_tics: usize,
    blink_tics: usize,
    tic: usize,
}

impl App {
    fn new(x: usize, y: usize) -> Self {
        let stdout = io::stdout();
        let backend = CrosstermBackend::new(stdout);
        let terminal = Terminal::new(backend).unwrap();
        let world = World::new(x, y);

        // let mut rng = rand::thread_rng();
        // for i in 0..world.grid.size().0 {
        //     for j in 0..world.grid.size().1 {
        //         let fl: f32 = rng.gen();
        //
        //         if fl > 0.5 {
        //             *world.grid.get_mut(i, j).unwrap() = automata::cells::Cell::Alive;
        //         }
        //     }
        // }

        let grid = world.grid.clone();

        App {
            mode: GameMode::Play,
            world,
            msgs: Vec::new(),
            terminal,
            petri_state: PetriState::new(grid),
            frame_tics: 3,
            blink_tics: 10,
            tic: 0,
        }
    }

    fn main_loop(&mut self) -> Result<(), io::Error> {
        let refresh = time::Duration::from_millis(16);
        loop {
            self.terminal.draw(|f| ui(f, &mut self.petri_state))?;

            match self.mode {
                GameMode::Play => {
                    thread::sleep(refresh);
                    if let Ok(b) = poll(refresh) {
                        if b {
                            if let Ok(event::Event::Key(key)) = read() {
                                if key.kind == KeyEventKind::Press {
                                    match key.code {
                                        KeyCode::Esc => break,
                                        KeyCode::Char(' ') => self.mode = GameMode::Edit,
                                        _ => (),
                                    }
                                }
                            }
                        }
                    }
                    if self.tic % self.frame_tics == 0 {
                        self.world.next_frame();
                    }
                }
                GameMode::Edit => {
                    thread::sleep(refresh);
                    if let Ok(b) = poll(refresh) {
                        if b {
                            if let Ok(event::Event::Key(key)) = read() {
                                if key.kind == KeyEventKind::Press {
                                    self.petri_state.blk = true;
                                    match key.code {
                                        KeyCode::Esc => break,
                                        KeyCode::Char(c) => match c {
                                            ' ' => self.mode = GameMode::Play,
                                            'h' => {
                                                self.petri_state.cursorpos.0 =
                                                    self.petri_state.cursorpos.0.saturating_sub(1)
                                            }
                                            'j' => {
                                                self.petri_state.cursorpos.1 = std::cmp::min(
                                                    self.petri_state.cursorpos.1 + 1,
                                                    self.world.grid.size().1 - 1,
                                                )
                                            }
                                            'k' => {
                                                self.petri_state.cursorpos.1 =
                                                    self.petri_state.cursorpos.1.saturating_sub(1)
                                            }
                                            'l' => {
                                                self.petri_state.cursorpos.0 = std::cmp::min(
                                                    self.petri_state.cursorpos.0 + 1,
                                                    self.world.grid.size().1 - 1,
                                                )
                                            }
                                            'a' => self.world.set_cell(
                                                self.petri_state.cursorpos.0,
                                                self.petri_state.cursorpos.1,
                                                Cell::Musical,
                                            ), // set cell
                                            _ => (),
                                        },
                                        _ => (),
                                    }
                                }
                            }
                        } else if self.tic % self.blink_tics == 0 {
                            self.petri_state.blk = !self.petri_state.blk;
                        }
                    }
                }
            }
            self.petri_state.cells = self.world.grid.clone();
            self.petri_state.mode = self.mode;
            self.tic = self.tic.wrapping_add(1);
        }

        Ok(())
    }
}

// fn main() -> Result<(), io::Error> {
//     // setup terminal
//     let mut stdout = io::stdout();
//     execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
//     enable_raw_mode()?;
//     let backend = CrosstermBackend::new(stdout);
//     let mut terminal = Terminal::new(backend)?;
//
//     let mut world = automata::world::World::new(100, 60);
//     let mut rng = rand::thread_rng();
//     for i in 0..world.grid.size().0 {
//         for j in 0..world.grid.size().1 {
//             let fl: f32 = rng.gen();
//
//             if fl > 0.4 {
//                 *world.grid.get_mut(i, j).unwrap() = automata::cells::Cell::Alive;
//             }
//         }
//     }
//
//     let refresh = time::Duration::from_millis(50);
//     let mut state = PetriState::new(world.grid.clone());
//
//     loop {
//         terminal.draw(|f| ui(f, &mut state))?;
//
//         thread::sleep(refresh);
//
//         if let Ok(b) = poll(refresh) {
//             if b {
//                 if let Ok(event::Event::Key(key)) = read() {
//                     match key.code {
//                         KeyCode::Esc => break,
//                         KeyCode::Down => {
//                             state.offset.1 = state.offset.1.saturating_add(1);
//                         }
//                         _ => (),
//                     }
//                 }
//             }
//         }
//
//         world.next_frame();
//         state.cells = world.grid.clone();
//     }
//
//     execute!(
//         terminal.backend_mut(),
//         LeaveAlternateScreen,
//         DisableMouseCapture
//     )?;
//     disable_raw_mode()?;
//     terminal.show_cursor()?;
//
//     Ok(())
// }

fn main() -> Result<(), io::Error> {
    // setup terminal
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    enable_raw_mode()?;

    let mut app = App::new(50, 100);
    app.main_loop()?;

    execute!(
        app.terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;

    disable_raw_mode()?;
    app.terminal.show_cursor()?;
    Ok(())
}
