use tui::style::Color;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Cell {
    Dead,
    Alive,
    Musical,
}

impl Default for Cell {
    fn default() -> Self {
        Cell::Dead
    }
}

impl Cell {
    pub(crate) fn rules(&self, nbh: Neighborhood) -> Cell {
        match self {
            Cell::Alive => Self::alive_rules(nbh),
            Cell::Dead => Self::dead_rules(nbh),
            Cell::Musical => Self::musical_rules(nbh),
        }
    }

    fn cnt(nbh: &Neighborhood, cell: Cell) -> u8 {
        let mut n = 0;
        for c in nbh.to_array().iter() {
            if let Some(a) = c {
                if **a == cell {n+=1}
            }
        }

        n
    }

    fn vn_cnt(nbh: &Neighborhood, cell: Cell) -> u8 {
        let mut n = 0;
        let vn_nbh = nbh.to_array();
        for i in (0..8).step_by(2) {
            if *vn_nbh[i].unwrap_or(&Cell::Dead) == cell {
                n += 1
            }
        }

        n
    }

    fn vn_cnt1(nbh: &Neighborhood, cell: Cell) -> u8 {
        let mut n = 0;
        let vn_nbh = nbh.to_array();
        for i in (0..8).step_by(2) {
            if *vn_nbh[i + 1].unwrap_or(&Cell::Dead) == cell {
                n += 1
            }
        }

        n
    }

    fn alive_rules(nbh: Neighborhood) -> Cell {
        match Cell::cnt(&nbh, Cell::Alive) + Cell::cnt(&nbh, Cell::Musical) {
            3 => Self::Musical,
            2 => Self::Alive,
            _ => Self::Dead
        }
    }
    fn dead_rules(nbh: Neighborhood) -> Cell {
        match Cell::cnt(&nbh, Cell::Musical) + Cell::cnt(&nbh, Cell::Alive) {
            3 => Cell::Alive,
            _ => Cell::Dead
        }
    }
    fn musical_rules(nbh: Neighborhood) -> Cell {
        match Cell::cnt(&nbh, Cell::Alive) + Cell::cnt(&nbh, Cell::Musical) {
            2 => Self::Musical,
            3 => Self::Alive,
            _ => Self::Dead
        }
    }
    pub fn color(&self) -> Color {
        match self {
            Cell::Dead => Color::Black,
            Cell::Alive => Color::Blue,
            Cell::Musical => Color::Red,
        }
    }
}

pub struct Neighborhood<'a> {
    pub(crate) nw: Option<&'a Cell>,
    pub(crate) n: Option<&'a Cell>,
    pub(crate) ne: Option<&'a Cell>,
    pub(crate) e: Option<&'a Cell>,
    pub(crate) se: Option<&'a Cell>,
    pub(crate) s: Option<&'a Cell>,
    pub(crate) sw: Option<&'a Cell>,
    pub(crate) w: Option<&'a Cell>,
}

impl Neighborhood<'_> {
    // returns an array of neighbors starting from north and going clockwise
    fn to_array(&self) -> [Option<&Cell>; 8] {
        [
            self.n, self.ne, self.e, self.se, self.s, self.sw, self.w, self.nw,
        ]
    }
}
