use std::cell::RefCell;

use grid::*;
use crate::automata::cells::*;

pub struct World {
    pub grid: Grid<Cell>,
}

impl World {
    pub fn new(x: usize, y: usize) -> Self {
        Self {
            grid: Grid::init(x, y, Cell::default())
        }

    }
    pub fn next_frame(&mut self) {
        let mut temp :Vec<Cell> = Vec::new();

        for x in 0..self.grid.size().0 {
            for y in 0..self.grid.size().1 {
                let nbh = Neighborhood {
                    n: if y != 0 {self.grid.get(x, y - 1)} else {None},
                    w: if x != 0 {self.grid.get(x - 1, y)} else {None},
                    nw: if x != 0 && y !=0 {self.grid.get(x -1, y -1)} else {None},
                    sw: if x != 0 {self.grid.get(x - 1, y + 1)} else {None},
                    ne: if y != 0 {self.grid.get(x + 1, y - 1)} else {None},
                    s: self.grid.get(x, y + 1),
                    e: self.grid.get(x + 1, y),
                    se: self.grid.get(x + 1, y + 1)
                };
                if let Some(cell) = self.grid.get(x, y) {
                    temp.push(cell.rules(nbh))
        }
            }
        }

        self.grid = Grid::from_vec(temp, self.grid.size().1);
    }
    pub fn set_cell(&mut self, x: usize, y: usize, cell: Cell) {

     if let Some(c) = self.grid.get_mut(x, y) {
        *c = cell;
     }

    }
}
